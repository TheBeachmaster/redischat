describe('Show Signup page', () => {
  it('Shows Signup page components', () => {
    cy.visit('/signup')
    cy.get('input#username').should('be.visible')
    cy.get('input#password').should('be.visible')
    cy.get('input#email').should('be.visible')
    cy.get('button').should('be.disabled')
  })
})
