describe('Post to Login', () => {
  it('Logs in', () => {
    cy.visit('/login')
    cy.get('button').should('be.disabled')
    cy.get('input#username').type('cypress')
    cy.get('input#password').type('cypress')
    cy.get('button').should('be.enabled')
    cy.get('button').click()
    cy.get('p#errorMsg').should('contain', 'User not found')
  })
})
