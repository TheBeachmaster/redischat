describe('Show login page', () => {
  it('Shows login page components', () => {
    cy.visit('/login')
    cy.get('input#username').should('be.visible')
    cy.get('input#password').should('be.visible')
    cy.get('button').should('be.disabled')
  })
})
