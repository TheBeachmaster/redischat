import { AuthProvider } from './auth'

const actor = require('comedy')

const actorSystem = actor()

export const AuthenticationActor = actorSystem.rootActor().then((rootActor) => {
  actorSystem
    .getLog()
    .info('Actor System Initalized for Authentication Service')
  return rootActor.createChild(AuthProvider)
})
