import { getAsync, ioRedisClient } from '../util/redis'
import { getUserKey } from '../util/utils'

export class AuthProvider {
  initialize(selfActor) {
    selfActor
      .getLog()
      .info('Authentication Service Actor initialized successfully!')
  }

  async createUserAccount(data) {
    let dbKey = getUserKey(data.username.toString())
    const saveUserDetails = await ioRedisClient.set(dbKey, JSON.stringify(data))
    return saveUserDetails
  }

  async lookupUserDetails(data) {
    let lookupKey = getUserKey(data.username.toString())
    let lookupResponse = {}
    const getUserData = await getAsync(lookupKey)
    if (!getUserData) {
      lookupResponse.res = 'NO_USER'
    } else {
      if (JSON.parse(getUserData).password == data.password) {
        lookupResponse.userData = {
          ...JSON.parse(getUserData),
        }
      } else {
        lookupResponse.res = 'WRONG_PASSWORD'
      }
    }
    return lookupResponse
  }
}
