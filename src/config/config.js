export const ApplicationConfig = {
  redis: {
    messageKeyPrefix: 'msg:',
    userDBKeyPrefix: 'user:',
    sessionWorkerKeyPrefix: 'worker:',
  },
}
