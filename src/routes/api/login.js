import { AuthenticationActor } from '../../handlers/main'
import { logger } from '../../util/logger'
import { UserSignInInputSchema } from '../../util/schema/user'

export async function post(req, res) {
  let signInData = JSON.parse(JSON.stringify(req.fields))
  const { error, value } = UserSignInInputSchema.validate(signInData)
  if (error === undefined) {
    logger.info(`[SIGNIN SCHEMA CHECK] Schema validation successfull`)

    AuthenticationActor.then((authProvider) => {
      authProvider.sendAndReceive('lookupUserDetails', value).then((reply) => {
        switch (reply.res) {
          case 'WRONG_PASSWORD':
            logger.warn(
              `[LOG IN] FAILURE Incorrect Password for \n ${JSON.stringify(
                signInData,
              )}`,
            )
            res
              .status(401)
              .send(JSON.stringify({ message: 'Password is incorrect' }))
            break
          case 'NO_USER':
            logger.warn(
              `[LOG IN] FAILURE Uknown user \n ${JSON.stringify(signInData)}`,
            )
            res.status(404).send(JSON.stringify({ message: 'User not found' }))
            break
          default:
            req.session.username = signInData.username
            logger.info(
              `[LOG IN] SUCCESS \n User:  ${JSON.stringify(
                reply,
              )} \n Session : ${JSON.stringify(req.session)}`,
            )
            res.sendStatus(200)
            break
        }
      })
    })
  } else {
    logger.error(
      `[SIGNIN SCHEMA CHECK] Schema validation failed. ${error.message}`,
    )
    res.status(400).send(JSON.stringify({ message: error.message }))
  }
}
