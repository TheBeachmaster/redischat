import { logger } from '../../util/logger'

export async function post(req, res) {
  let sessionInfo = req.fields
  logger.info(`[LOGOUT] Logging out ${JSON.stringify(sessionInfo)}`)
  delete req.session
  res.status(200).send('OK')
}
