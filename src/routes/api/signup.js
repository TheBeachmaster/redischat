import { logger } from '../../util/logger'
import { userAccountCreationWorker } from '../../util/queue'
import { UserSignupInputSchema } from '../../util/schema/user'

export async function post(req, res) {
  let signUpData = JSON.parse(JSON.stringify(req.fields))
  const { error, value } = UserSignupInputSchema.validate(signUpData)
  if (error === undefined) {
    logger.info(`[SIGNUP SCHEMA CHECK] Schema validation successfull`)
    userAccountCreationWorker.add('create-user', value, {
      removeOnComplete: true,
    })
    res
      .status(200)
      .send(JSON.stringify({ message: 'Account Creation in progress!' }))
  } else {
    logger.error(
      `[SIGNUP SCHEMA CHECK] Schema validation failed. ${error.message}`,
    )
    res.status(401).send(JSON.stringify({ message: error.message }))
  }
}
