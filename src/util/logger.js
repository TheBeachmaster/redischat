const winston = require('winston')
const winstonFileRotate = require('winston-daily-rotate-file')

let httpErrorTransport = new winstonFileRotate({
  filename: 'log/http-error-%DATE%.log',
  datePattern: 'YYYY-MM-DD-HH',
  zippedArchive: true,
  maxSize: '1m',
  level: 'error',
  timestamp: true,
  exitOnError: false,
  handleExceptions: true,
})

let httpInfoTransport = new winstonFileRotate({
  filename: 'log/http-info-%DATE%.log',
  datePattern: 'YYYY-MM-DD-HH',
  zippedArchive: true,
  maxSize: '1m',
  timestamp: true,
  exitOnError: false,
  handleExceptions: true,
})

let errorTransport = new winstonFileRotate({
  filename: 'log/application-error-%DATE%.log',
  datePattern: 'YYYY-MM-DD-HH',
  zippedArchive: true,
  maxSize: '1m',
  level: 'error',
  timestamp: true,
  exitOnError: false,
  handleExceptions: true,
})

let serviceTransport = new winstonFileRotate({
  filename: 'log/application-info-%DATE%.log',
  datePattern: 'YYYY-MM-DD-HH',
  zippedArchive: true,
  maxSize: '1m',
  timestamp: true,
  exitOnError: false,
  handleExceptions: true,
})

export const logger = winston.createLogger({
  level: 'info',
  defaultMeta: { service: 'chat-service' },
  transports: [
    new winston.transports.Console({ handleExceptions: true }),
    serviceTransport,
    errorTransport,
  ],
  format: winston.format.combine(
    winston.format.json(),
    winston.format.timestamp(),
    winston.format.prettyPrint(),
  ),
})

if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({ handleExceptions: true }))
}

export const httpErrorLogger = httpErrorTransport
export const httpInfoLogger = httpInfoTransport
