const Joi = require('joi')

export const MessageBodySchema = Joi.object({
  message: Joi.string().min(1).alphanum().required(),
  sender: Joi.string().required(),
  channel: Joi.string().min(6).required(),
  messageId: Joi.string().guid().required(),
  timestamp: Joi.date().timestamp('unix'),
})
