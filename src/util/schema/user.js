const Joi = require('joi')

export const UserSignupInputSchema = Joi.object({
  email: Joi.string().email().required(),
  username: Joi.string().required(),
  password: Joi.string().min(6).required(),
})

export const UserSignInInputSchema = Joi.object({
  username: Joi.string().required(),
  password: Joi.string().min(6).required(),
})
