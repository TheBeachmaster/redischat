const Redis = require('ioredis')
const { promisify } = require('util')

const getRedisConnectionClient = () => {
  let redisConnOpts = {}
  redisConnOpts.opts = {}
  if (process.env.NODE_ENV == 'production') {
    redisConnOpts.port = `${process.env.REDIS_PORT}`
    redisConnOpts.host = `${process.env.REDIS_HOST}`
    redisConnOpts.password = `${process.env.REDIS_PASS}`
    redisConnOpts.tls = true
  } else {
    redisConnOpts.port = 6379
    redisConnOpts.host = '127.0.0.1'
  }
  redisConnOpts.opts.lazyConnect = false
  redisConnOpts.opts.connectionName = 'redischat'
  return redisConnOpts
}

let redisConnString = getRedisConnectionClient()

const localRedisClient = new Redis(
  redisConnString.port,
  redisConnString.host,
  redisConnString.opts,
)

export const getIORedisOptions = () => getRedisConnectionClient()

export const ioRedisClient = localRedisClient
export const hgetallAsync = promisify(localRedisClient.hgetall).bind(
  localRedisClient,
)
export const getAsync = promisify(localRedisClient.get).bind(localRedisClient)
