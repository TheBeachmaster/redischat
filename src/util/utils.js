import { v5 as uuidv5 } from 'uuid'
import { ApplicationConfig } from '../config/config'

export const getUserKey = (username) => {
  const NAME_SPACE = '1d3bed34-223a-49ad-8e79-844518424212'
  let userNameUUID = uuidv5(username, NAME_SPACE)
  let userDBKey = (ApplicationConfig.redis.userDBKeyPrefix += userNameUUID)
  return userDBKey
}
