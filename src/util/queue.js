const Queue = require('bull')
const Redis = require('ioredis')
import { getIORedisOptions } from './redis'

let subscriber = new Redis(
  getIORedisOptions().port,
  getIORedisOptions().host,
  getIORedisOptions().opts,
)
let client = new Redis(
  getIORedisOptions().port,
  getIORedisOptions().host,
  getIORedisOptions().opts,
)

const opts = {
  createClient: (type) => {
    switch (type) {
      case 'client':
        return client
      case 'subscriber':
        return subscriber
      case 'bclient':
        return new Redis(
          getIORedisOptions().port,
          getIORedisOptions().host,
          getIORedisOptions().opts,
        )
      default:
        throw new Error('Unexpected connection type: ', type)
    }
  },
}

let accountManager, messageServer

accountManager = new Queue('account-service', opts)

messageServer = new Queue('message-service', opts)

export const userAccountCreationWorker = accountManager
export const messageServiceWorker = messageServer
