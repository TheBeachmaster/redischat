const cluster = require('cluster')
import compression from 'compression'
import * as sapper from '@sapper/server'
import { httpErrorLogger, httpInfoLogger, logger } from './util/logger'
import { userAccountCreationWorker } from './util/queue'
import { AuthenticationActor } from './handlers/main'
import { ioRedisClient } from './util/redis'
const winston = require('winston')
const expressWinston = require('express-winston')
const formidableMiddleware = require('express-formidable')
const cors = require('cors')
const numCPUs = require('os').cpus().length
const session = require('express-session')
const RedisStore = require('connect-redis')(session)
const formidableOpts = {
  keepExtensions: true,
}

const formidableEvts = [
  {
    event: 'error',
    action: function (error) {
      logger.error(`An error ocurred ${error} `)
    },
  },
  {
    event: 'aborted',
    action: function () {
      logger.error(`Upload aborted`)
    },
  },
]

if (cluster.isMaster) {
  logger.info(`Master is running as ${process.pid} `)
  logger.info(`Creating ${numCPUs} workers`)
  for (let index = 0; index < numCPUs; index++) {
    cluster.fork()
  }
  cluster.on('exit', (worker, code, signal) => {
    logger.error(
      `Worker ${worker.id} died due to ${signal} with exit code ${code}`,
    )
    logger.info('Creating a new worker after death')
    cluster.fork()
  })
  cluster.on('listening', (worker, address) => {
    logger.info(
      `Worker ${worker.id} is gossiping on ${address.address}:${address.port}`,
    )
  })
  cluster.on('fork', (worker) => {
    logger.info(`Started a new Worker with ID: ${worker.id}`)
  })
  cluster.on('online', (worker) => {
    logger.info(`Worker ${worker.id} is online and ready to process requests`)
  })
  cluster.on('message', (worker, message, handle) => {
    logger.info(
      `Message from worker ${worker.id}. Message data ${JSON.stringify(
        message,
      )}`,
    )
  })
} else {
  const express = require('express')

  const app = express()

  app.use(cors())

  app.use(
    expressWinston.logger({
      transports: [new winston.transports.Console(), httpInfoLogger],
      format: winston.format.combine(
        winston.format.json(),
        winston.format.timestamp(),
        winston.format.prettyPrint(),
      ),
      requestWhitelist: [
        'url',
        'headers',
        'method',
        'httpVersion',
        'originalUrl',
        'query',
        'fields',
      ],
      responseWhitelist: ['body'],
    }),
  )
  app.use(
    expressWinston.errorLogger({
      transports: [new winston.transports.Console(), httpErrorLogger],
      format: winston.format.combine(
        winston.format.json(),
        winston.format.timestamp(),
        winston.format.prettyPrint(),
      ),
      requestWhitelist: [
        'url',
        'headers',
        'method',
        'httpVersion',
        'originalUrl',
        'query',
        'fields',
      ],
      responseWhitelist: ['body'],
    }),
  )

  app.use(
    session({
      secret: 'redischat',
      resave: false,
      rolling: true,
      saveUninitialized: false,
      cookie: {
        maxAge: 14400000,
        secure: process.env.NODE_ENV === 'production',
        signed: process.env.NODE_ENV === 'production',
      },
      store: new RedisStore({
        client: ioRedisClient,
        prefix: 'redischat:',
      }),
    }),
  )

  app.use(
    express.static('static'),
    compression({ threshold: 0 }),
    formidableMiddleware(formidableOpts, formidableEvts),
    sapper.middleware({
      session: (req, res) => ({
        username: req.session && req.session.username,
        sessionid: req.sessionID,
        // email: req.session.email,
        // token: req.session.token,
        // userid: req.session.userID,
      }),
    }),
  )

  const appPort = 14450

  app.listen(process.env.PORT || appPort, () => {
    logger.info(
      `Redis Chat Application (re)started at ${Date.now().toString()}!`,
    )
  })

  userAccountCreationWorker.process('create-user', async (job, done) => {
    logger.info(`[USER ACCOUNT CREATION WORKER] ${job.id} is running`)
    let data = job.data
    AuthenticationActor.then((authProvider) => {
      authProvider.sendAndReceive('createUserAccount', data).then((reply) => {
        logger.info(
          `[USER ACCOUNT CREATION WORKER] Account Creation Result${reply} `,
        )
      })
    })
    logger.info(`[USER ACCOUNT CREATION WORKER] ${job.id} is done`)
    userAccountCreationWorker.close()
    done()
  })
}
