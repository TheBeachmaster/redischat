#!/bin/bash

COMMAND=$1
TARGET=registry.heroku.com
IMAGE=$IMAGE_NAME
TAG=$BUILD_TAG
PROCESS=web

export BUILD=$TARGET/$IMAGE/$PROCESS 

function docker_login {
    echo "====== Authenticating to Registry ======= \n\n"
    echo "Username: ${REGISTRY_USER_TOKEN} Password: ${REGISTRY_USER_TOKEN} Target: ${TARGET}"
    docker login --username ${REGISTRY_USER_TOKEN} --password ${REGISTRY_USER_TOKEN} ${TARGET}
    echo "======= Done logging in ====== \n\n"
}

function docker_build {
    echo "Building====== $BUILD ======= \n\n"
    docker build -t ${BUILD} .
    echo "Done Building====== $BUILD ======= \n\n"
}

function docker_push {
    echo "Pushing====== $BUILD ======= \n\n"
    docker push ${BUILD}
    echo "Done Pushing====== $BUILD ======= \n\n"
}

if [[ $COMMAND == 'build' ]]; then
    docker_build
elif [[ $COMMAND == 'push' ]]; then
    docker_push
elif [[ $COMMAND == 'login' ]]; then
    docker_login
else
    echo "Unknown Command $COMMAND"
    exit 113
fi